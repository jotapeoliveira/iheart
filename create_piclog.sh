#!/bin/bash

if [ -f "/home/root/piclogs.py" ]; then
    echo "Files already created!"
    exit 1
fi

DIR=/usr/local/www/current/piclog

mkdir -p ${DIR}

cat > ${DIR}/index.html <<EOF
<html>                                                                       
<head>                                                           
   <meta http-equiv="refresh" content="0; url=/piclog/piclog.cgi" /> 
</head>                                                              
<body>                                                               
   <p><a href="/piclog/piclog.cgi">Redirect</a></p>                  
</body>                                                              
</html>    
EOF

cat > ${DIR}/piclog.cgi <<EOF
<% . /usr/local/lib/cgi/haserl.sh ; check_instances piclog.cgi %>                                                                                                                                                                                                               
<% . /usr/local/lib/cgi/generic.sh ; print_http_hdr %>                                                                                                                                                                                                                          
<% . /usr/local/lib/cgi/status.sh %>                                                                                                                                                                                                                                            
<% . /usr/local/lib/cgi/config.sh %>                                                                                                                                                                                                                                            
<%                                                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                
function print_log()                                                                                                                                                                                                                                                            
{                                                                                                                                                                                                                                                                               
  # number of last entries to be shown on the homepage                                                                                                                                                                                                                          
  LOG_HISTORY_LEN=100                                                                                                                                                                                                                                                           
  echo -n '<form action="'\${3}'" method="get" style="margin-block-end: 0; height: 36px;">'                                                                                                                                                                                      
  echo -n '    <table width="100%"><tr>'                                                                                                                                                                                                                                        
  echo -n '        <tr>'                                                                                                                                                                                                                                                        
  echo -n '            <td style="font-weight: bold; font-size: 14px; color: #444444; vertical-align: middle; width:120px;"><b>'${1}'</b></td>'                                                                                                                                 
  echo -n '            <td style="width: 1%; padding-right: 20px;"><input type="button" onclick="window.location.reload()" value=" Refresh "</td>'                                                                                                                              
  echo -n '        </tr>'                                                                                                                                                                                                                                                       
  echo -n '    </table>'                                                                                                                                                                                                                                                        
  echo -n '</form>'                                                                                                                                                                                                                                                             
                                                                                                                                                                                                                                                                                
  echo '<div class="logs-canvas" style="background-color: #222222;                                                                                                                                                                                                              
                                        color: #CCCCCC;                                                                                                                                                                                                                         
                                        border: 1px solid #CCCCCC;                                                                                                                                                                                                              
                                        overflow:auto;                                                                                                                                                                                                                          
                                        height: calc(100vh - 48px);                                                                                                                                                                                                             
                                        width: calc(100vw - 16px);                                                                                                                                                                                                              
                                        font-size:12px;" id="logscanvas">'                                                                                                                                                                                                      
  echo '<pre class="logs-canvas" style="margin-block-start: 0.5em;                                                                                                                                                                                                              
                                        margin-block-end: 0.5em;                                                                                                                                                                                                                
                                        margin-inline-start: 0.5em;">'                                                                                                                                                                                                          
  tail -n \$LOG_HISTORY_LEN "\$2"                                                                                                                                                                                                                                                 
  echo '</pre></div>'                                                                                                                                                                                                                                                           
}                                                                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                                
%>                                                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                                                
<html>                                                                                                                                                                                                                                                                          
<head>                                                                                                                                                                                                                                                                          
    <!-- <meta http-equiv="refresh" content="1"> -->                                                                                                                                                                                                                            
    <link href="/sys/css/basic.css" rel="stylesheet" type="text/css">                                                                                                                                                                                                           
    <link href="/css/ipformer.css" rel="stylesheet" type="text/css">                                                                                                                                                                                                            
    <style type="text/css">                                                                                                                                                                                                                                                     
      body { margin:0 0 0 8px; }                                                                                                                                                                                                                                                
    </style>                                                                                                                                                                                                                                                                    
    <script language="javascript">                                                                                                                                                                                                                                              
        function scrollLogsToBottom() {                                                                                                                                                                                                                                         
            var logsDiv = document.getElementById("logscanvas");                                                                                                                                                                                                                
            logsDiv.scrollTop = logsDiv.scrollHeight;                                                                                                                                                                                                                           
        }                                                                                                                                                                                                                                                                       
    </script>                                                                                                                                                                                                                                                                   
</head>                                                                                                                                                                                                                                                                         
<body onload="scrollLogsToBottom();">                                                                                                                                                                                                                                           
    <%                                                                                                                                                                                                                                                                          
        print_log "PIC log" /var/log/pic.log ../cgi-bin/download_applog.cgi                                                                                                                                                                                                     
    %>                                                                                                                                                                                                                                                                          
</body>                                                                                                                                                                                                                                                                         
</html>  
EOF


cat > /home/root/piclogs.py <<EOF
import serial
from datetime import datetime
import threading
import time
import sys
import logging

#isLocked = threading.Lock()

def write_to_serial(ser):
    
    while True:
        ser.write("C1\n".encode('utf-8'))
        time.sleep(3)

def read_from_serial(ser):

    logging.basicConfig(format='%(levelname)s:%(message)s',filename='/var/log/pic.log', level=logging.INFO)
    logging.info("INIT")
    while True:
        now = datetime.now()
        current_time = now.strftime("%b %d, %H:%M:%S")
        data_raw = ser.readline().encode('utf-8')[:-2]
        if data_raw != '':
            new_data = data_raw.strip("\r\n")
            logging.info(current_time + " PIC Status: " + new_data)
        time.sleep(0.01)
        

def Main():
   
    ser = serial.Serial('/dev/ttyACM0',115200)
    t1 = threading.Thread(target = write_to_serial, args=[ser])
    t2 = threading.Thread(target = read_from_serial, args=[ser])
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    

if __name__ == '__main__':
    Main()
    
EOF

chmod +x /home/root/piclogs.py

echo "Files for PIC logs created!"
