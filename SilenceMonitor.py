import alsaaudio as audio
import time
import audioop
import threading

'''
Module to monitor silence of Audio Chanel for Iheart
'''

class SilenceMonitor:

    def __init__(self, threshold=1, cb=None, maxSilenceTimeToCb_ms=1000, audiocard = 'hw:0,0', isRecursiveCallback = False):
        self.audiocard= audiocard
        self.threshold = threshold
        self.maxSilenceTimeToCb = maxSilenceTimeToCb_ms
        self.cb = cb
        self.isRecursiveCallback =isRecursiveCallback
        #Capture Settings
        self.PERIOD_SIZE = 160
        self.AUDIO_FORMAT = audio.PCM_FORMAT_S16_LE
        self.CHANNELS = 1
        self.FRAMERATE=8000
        self.LOOP_SLEEP_TIME_S = 0.05
        #set capture dev
        self.configureCapture()
        self.monitorThread = None                       #Thread object Holder
        self.stopThread = False                         #flag to stop thread
        self.accumulatedSilenceTime_ms = 0
        self.semaphore = threading.Lock()



    def configureCapture(self):
        self.inp = audio.PCM(audio.PCM_CAPTURE,audio.PCM_NONBLOCK,device=self.audiocard)
        self.inp.setchannels(self.CHANNELS)
        self.inp.setrate(self.FRAMERATE)
        self.inp.setformat(self.AUDIO_FORMAT)
        self.inp.setperiodsize(self.PERIOD_SIZE)

    #Stop the streaming process and wait for secondary thread to join main thread
    def stopMonitoring(self, block = True):
        if not self.monitorThread == None:
            print 'Stopping Silence Monitor'
            self.stopThread = True
            if block:
                self.monitorThread.join()
                self.monitorThread = None
            else:
                aux_monitorThread = self.monitorThread
                self.monitorThread = None
                return aux_monitorThread


    #Start the Record and streaming process in a thread
    def startMonitoring(self):
        if self.monitorThread == None:
            print 'Starting Silence Monitor'
            self.monitorThread = threading.Thread(target=self.monitorThreadWorker)
            self.monitorThread.start()
            return self.monitorThread
            

        
    def getSilenceTime(self):
        self.semaphore.acquire()
        val = self.accumulatedSilenceTime_ms
        self.semaphore.release()
        return val
        


    #Thread Worker. Can be called without thread and will block code execution. To start without blocking, call startRecAndStream()
    def monitorThreadWorker(self):
        callCb = True
        isFirstIteration = True
        init_time_ms = time.time()*1000
        while not self.stopThread:
            try:
                length, buf = self.inp.read()
                if length and length==self.PERIOD_SIZE:
                    #print 'Got Length: {}'.format(length)
                    maxVal = audioop.max(buf, 2)     #16 bits --> width=2
                    #print 'Max is: {}',format(maxVal)
                    
                    if maxVal>=self.threshold:
                        #print "Not Silence"
                        init_time_ms = time.time()*1000
                        callCb = True
                        # self.accumulatedSilenceTime_ms = 0
                        
                    #else:
                        #print "Silence"

                    self.semaphore.acquire()
                    self.accumulatedSilenceTime_ms = time.time()*1000 - init_time_ms
                    self.semaphore.release()


                # if silence time gt max allowed, call cb
                #print'Tot Silnce Time is {} '.format(self.accumulatedSilenceTime_ms)
           
                if (self.accumulatedSilenceTime_ms>self.maxSilenceTimeToCb) and self.cb is not None:
                    if callCb or self.isRecursiveCallback:
                        self.cb(self.accumulatedSilenceTime_ms)
                        init_time_ms = time.time()*1000
                        callCb = False
                
                time.sleep(self.LOOP_SLEEP_TIME_S)

                
        

            except Exception as e:
                # pass
                print 'Exception Triggered: {}'.format(e)

        self.stopThread = False


            
        

if __name__ == "__main__":
    def myCb(silenceTime):
        print 'Silence time ---------> {}'.format(silenceTime)
        

    mySilenceMonitor = SilenceMonitor(50, myCb,)
    mySilenceMonitor.startMonitoring()

    while True:
        print 'Tot Silence Time is: {}'.format(mySilenceMonitor.getSilenceTime())
        time.sleep(2)