SILENCE_THRESHOLD = 50
MAX_SILENCE_TIME_MS = 10000
CAPTURE_DEV = 'hw:0,0' #plug:dsnooped
PIC_SERIAL_PORT = '/dev/ttyACM0'
INTEL_SERIAL_PORT = '/dev/ttyS1'
PIC_BAUDRATE = 115200
SAMPLING_PERIOD = 1 #sampling period in SECONDS