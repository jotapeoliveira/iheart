import serial
from datetime import datetime
import threading
import time
import sys
import logging
import os
filepath = '/tmp/vumeter'
word = 'Max peak'
sound = False
data = ''

def scope_audio(ser):
	global sound
	global cmd	
	os.system('arecord -d 10 -D "hw:0,0" -f cd -vvv -V stereo 2> /tmp/vumeter  1>/dev/null ')
	with open(filepath) as fp:
		line = fp.readline()
		cnt = 1
		while line:
			if word in line:
				if int(line.split('# ',1)[1].strip()[:-1]) > 0 and cnt > 1:
					sound = True
				cnt += 1
			line = fp.readline()
	fp.close()
	if sound == False:
		print 'no sound'
			
	else: 
		
		print 'sound'

def write_to_serial(ser,cmd):
        ser.write(cmd.encode('utf-8'))
        time.sleep(0.01)

def read_from_serial(ser):
	global data_raw
    	#logging.basicConfig(format='%(levelname)s:%(message)s',filename='/var/log/pic.log', level=logging.INFO)
    	#logging.info("INIT")
	#while True:
        #now = datetime.now()
        #current_time = now.strftime("%b %d, %H:%M:%S")
        data = ser.readline().encode('utf-8')[:-2]	
        if data == '':
		print 'Error while reading serial connection'
        #    	new_data = data_raw.strip("\r\n")
        #    	logging.info(current_time + " PIC Status: " + new_data)
        time.sleep(0.01)
	return data
        
def sampling():
	time.sleep(15)
def Main():
	global sound
	count = 0
	while True:	   
		ser = serial.Serial('/dev/ttyACM0',115200)
    		t1 = threading.Thread(target = scope_audio, args=[ser])
    		t4 = threading.Thread(target = sampling, args=[])
		t4.start()
		write_to_serial(ser,'R\n')
		data = read_from_serial(ser)
		print data
		if data[1]=='1':
			t1.start()
			t1.join()
			if sound == False:
				write_to_serial(ser,'C0\n')
		else:
			if count >= 3:
				write_to_serial(ser,'C1\n')
				count = 0
			count = count + 1
		t4.join()
		sound = False
		
if __name__ == '__main__':
    	Main()
    
