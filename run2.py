from SilenceMonitor import SilenceMonitor
from Constants import *
import threading
import serial
import time
import json


def write_to_serial(ser,cmd):
    ser.write(cmd.encode('utf-8'))
    time.sleep(0.01)
def read_from_serial(ser):
    data = ser.readline().encode('utf-8')[:-2]	
    time.sleep(0.01)
    return data
def sampling(SAMPLING_PERIOD):
    time.sleep(SAMPLING_PERIOD)
def json_data_creator(data, datajson):
    datajson["My PIC ID:"]=data[1:9]
    if data[10]=='1':
        datajson["My state:"]="Transmitting"
    else:
        datajson["My state:"]="Not transmitting"   
    datajson["PIC signal from Studio Hub:"]=data[12:16]
    datajson["PIC signal to Studio Hub:"]=data[17:21]
    datajson["PIC current to UP Board:"]=data[22:26]
    if data[30] == '1':
        datajson["PIC SPDIF state:"]="Enable"    
    else: 
        datajson["PIC SPDIF state:"]="Disable"  
    datajson["PIC temp sensor 1:"]=data[32:36] 
    datajson["PIC temp sensor 2:"]=data[37:41]  
    datajson["PIC temp internal:"]=data[42:46] 
    if data[47:55] == '00000000':
        datajson["Paralel device:"]="No device connected"  
        datajson["Paralel device state:"]="No info" 
    else:
        datajson["Paralel device:"]=data[47:55] 
        if data[56] == '1':
            datajson["Paralel device state:"]="Transmitting"
        else:
            datajson["Paralel device state:"]="Not Transmitting" 
    datajson=json.dumps(datajson)
    return datajson
            
def Main():
    maxattempts = 10 # Number of attempts to try read the status from PIC
    datajson = {}
    excCtr=0
    myMonitor = SilenceMonitor(SILENCE_THRESHOLD,None,None,CAPTURE_DEV)
    myMonitor.startMonitoring()
    serpic = serial.Serial('/dev/ttyACM0',115200, serial.EIGHTBITS, serial.PARITY_NONE,serial.STOPBITS_ONE, timeout=0, write_timeout=0)
    #serintel = serial.Serial('/dev/ttyS1',9600, timeout=2.5,parity=serial.PARITY_NONE,bytesize=serial.EIGHTBITS,stopbits=serial.STOPBITS_ONE)
    serintel = serial.Serial('/dev/ttyS1',115200, serial.EIGHTBITS, serial.PARITY_NONE,serial.STOPBITS_ONE, timeout=0, write_timeout=0)
    while True:
        start_time=time.time()
        
        write_to_serial(serpic, 'R\n')
        data = read_from_serial(serpic)
        if data == None or len(data)!= 57:
            for i in range(maxattempts):
                write_to_serial(serpic, 'R\n')
                data = read_from_serial(serpic)
                if len(data)== 57:
                    break
        if len(data) == 57:
            print data
            #DataToIntel = json_data_creator(data, datajson)
            #serintel.write(DataToIntel.encode('ascii'))
            #serintel.flush()
            #try:
            #    incoming = serintel.readline().decode("utf-8")
            #    print (incoming)
            #except Exception as e:
            #    print (e)
            #    pass
            write_to_serial(serintel,data+'\r\n')
            if data[10] == '0' :
                write_to_serial(serintel,'Situation 1'+'\r\n')
                print 'Situation 1'   
            elif data[10] == '1' and data[47:55] == '00000000':
                write_to_serial(serintel,'Situation 2'+'\r\n')
                print 'Situation 2'   
            elif data[10] == '1' and data[47:55] != '00000000':
                if myMonitor.getSilenceTime() > MAX_SILENCE_TIME_MS:
                    write_to_serial(serpic,'C0\n')
                    write_to_serial(serintel,'Situation 3'+'\r\n')
                    print 'Comand sent'    
                print 'Situation 3'              
        else:
            print 'Corrupted loop'
        while time.time() - start_time < SAMPLING_PERIOD:
           pass
if __name__ == '__main__':
    	Main()        
        