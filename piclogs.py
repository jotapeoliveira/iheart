import serial
from datetime import datetime
import threading
import time
import sys
import logging

#isLocked = threading.Lock()

def write_to_serial(ser):
    
    while True:
        ser.write("C1\n".encode('utf-8'))
        time.sleep(3)

def read_from_serial(ser):

    logging.basicConfig(format='%(levelname)s:%(message)s',filename='/var/log/pic.log', level=logging.INFO)
    logging.info("INIT")
    while True:
        now = datetime.now()
        current_time = now.strftime("%b %d, %H:%M:%S")
        data_raw = ser.readline().encode('utf-8')[:-2]
        if data_raw != '':
            new_data = data_raw.strip("\r\n")
            logging.info(current_time + " PIC Status: " + new_data)
        time.sleep(0.01)
        

def Main():
   
    ser = serial.Serial('/dev/ttyACM0',115200)
    t1 = threading.Thread(target = write_to_serial, args=[ser])
    t2 = threading.Thread(target = read_from_serial, args=[ser])
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    

if __name__ == '__main__':
    Main()