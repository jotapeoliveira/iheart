from SilenceMonitor import SilenceMonitor
from Constants import *
import threading, time, serial, os

class MachineState:
    def __init__(self):
        pass
    INIT = 0 
    MASTER_ALONE = 1
    MASTER_NOT_ALONE = 2
    SLAVE = 3

def serialPollThreadWorker(serialHandler, callback):
    global stopSerialPoll
    print("Starting serialPollThreadWorker")
    while not stopSerialPoll:
        msg= ""
        while not stopSerialPoll:
            char = serialHandler.read(1).decode('ascii')
            # print("Char received: {}".format(char))
            if char != '\n' and char != '\r':
                msg+=char
 #           elif char =='\r':
 #               pass
            elif char== '\n':
                # print("New message: {}".format(msg))
                callback(msg)
                break
    stopSerialPoll = False

        
def picMsgParser(msg):
    global iAmMaster, lastReceivedMsg, iAmTheOnlyOne
    #print 'MsgRcv: {}'.format(msg)
    #check if msg is valid
    if len(msg)!=57:
        #print 'Invalid Len'
        return

    paramsArr=msg.split(',')
    if len(paramsArr)!=11:
        #print 'paramsArray len dont match'
        return

    lastReceivedMsg = msg
    intelSerHandler.write((msg+'\r\n').encode('utf-8'))
    #check if i am master
    if paramsArr[1] == '1':  #Verifies the second parameter from the PIC answer to the command 'R\n'
        iAmMaster = True

    elif paramsArr[1] == '0':
        iAmMaster = False

    #check if alone
    if paramsArr[9]=='00000000':
        iAmTheOnlyOne = True
    else:
        iAmTheOnlyOne = False


#called whenever SilenceTime reaches MAX_SILENCE_TIME_MS (defined in Constants.py)
def silenceCb(tim):
    global iAmMaster, iAmTheOnlyOne, stateTransitionTime
    #print 'Silence Time: {}'.format(tim)
    if tim>=MAX_SILENCE_TIME_MS and not iAmTheOnlyOne:
        if iAmMaster:
            iAmMaster = False
            stateTransitionTime = time.time()

#on state machine state chnge
def onStateChange():
    print 'New St --> {}'.format(currState)

currState = MachineState.INIT
previousState = MachineState.INIT
stopSerialPoll = False
lastReceivedMsg = ''
iAmMaster = False
iWasMaster = False
iAmTheOnlyOne = True
iWasTheOnlyOne = True
stateTransitionTime = None
picRequestTime = 0

os.system('killall -9 barix_watchdog')
time.sleep(1)
os.system('killall -9 audiostreamer')
time.sleep(1)
#create Silence monitor instance
myMonitor = SilenceMonitor( 50, silenceCb, MAX_SILENCE_TIME_MS, CAPTURE_DEV, True)
#open serialPorts
picSerHandler = serial.Serial(PIC_SERIAL_PORT, PIC_BAUDRATE)
intelSerHandler = serial.Serial(INTEL_SERIAL_PORT, PIC_BAUDRATE)
picSerialThread = threading.Thread(target=serialPollThreadWorker, args=(picSerHandler, picMsgParser ))
picSerialThread.start()
#start silenceMonitor thread
myMonitor.startMonitoring()

while True:
    time.sleep(0.1)
    time_now = time.time()
    if time_now-picRequestTime>SAMPLING_PERIOD:
        picSerHandler.write("R\n".encode('utf-8'))
        picRequestTime = time_now
    
    if iAmMaster!= iWasMaster:
        print 'iAmMaster = {}'.format(iAmMaster)
        iWasMaster = iAmMaster

    if iAmTheOnlyOne!= iWasTheOnlyOne:
        print 'iAmTheOnlyOne = {}'.format(iAmTheOnlyOne)
        iWasTheOnlyOne = iAmTheOnlyOne

    #alert state changes
    if previousState!= currState:
        onStateChange()
        previousState = currState


    if currState==MachineState.INIT:
        #request status
        picSerHandler.write('R\n'.encode('utf-8'))
        time.sleep(1)
        #parse
        #Block until PIC answers
        while lastReceivedMsg=='':
            time.sleep(1)
            if lastReceivedMsg=='':
                picSerHandler.write('R\n'.encode('utf-8'))
                time.sleep(1)

        #change state
        if iAmMaster and iAmTheOnlyOne :
            print 'I will be MASTER ALONE'
            currState = MachineState.MASTER_ALONE
        elif iAmMaster and not iAmTheOnlyOne:
            print 'I will be MASTER NOT ALONE'
            currState = MachineState.MASTER_NOT_ALONE
        else:
            print 'i WILL BE SLAVE'
            currState = MachineState.SLAVE


    # faz o mesmo que slave
    if currState == MachineState.MASTER_ALONE:
        if not iAmMaster:
            #in this case PIC will take the decision
            currState = MachineState.SLAVE
        elif iAmMaster and not iAmTheOnlyOne:
            currState = MachineState.MASTER_NOT_ALONE

    # monitor audio. If silenceTime>=X --> pass to slave
    if currState == MachineState.MASTER_NOT_ALONE:
        if not iAmMaster:
            picSerHandler.write('C0\n'.encode('utf-8'))
            print 'Sending Slave msg'
            currState = MachineState.SLAVE

        elif iAmMaster and iAmTheOnlyOne:
            currState = MachineState.MASTER_ALONE



    # 
    elif currState == MachineState.SLAVE:
        if iAmMaster and iAmTheOnlyOne:
            currState= MachineState.MASTER_ALONE
        elif iAmMaster and not iAmTheOnlyOne:
            currState = MachineState.MASTER_NOT_ALONE
